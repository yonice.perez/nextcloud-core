<?php
namespace OCA\ECloud\Core;

/**
 * @example
 */
class ValidatorCore extends \GUMP
{
	public function __construct()
	{
		parent::__construct('en');
		
		$this->add_validator("valid_emails", function($field, $input, $param = NULL) {
		   	
		    $splitStr = explode(",", $input[$field]);

		    foreach ($splitStr as $value) {
		    	if(!filter_var( trim($value), FILTER_VALIDATE_EMAIL))
				{
					return false;
				}	
		    }

		    return true;
		    
		}, 'Invalid email' );
	}

	
}