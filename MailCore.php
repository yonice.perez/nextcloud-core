<?php
namespace OCA\ECloud\Core;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\Mail\IMailer;
/**
 * @example
 * 	$mail = new MailCore('yonice.perez@gmail.com,jonathan@greenshieldtech.com');
 * 	$mail->addAttach( $pdf->output(), 'filename.pdf');
 * 	$mail->send( '<h1>Hello World</h1>' , 'Subject X' );
 * 
 */
class MailCore {
	

	/** @var IMailer $_mailer **/
	private $_mailer;
	
	/** @var Array $_attachments **/
	private $_attachments = [];

	/** @var Array $_emailAddress **/
	private $_emailAddress = [];
	
	/**
	 * @param String $address
	 * @param String $generalName
	 */
	public function __construct( String $address = '', String $defaultName  = '')
	{
		if( $address )
		{
			$arrEmails = explode(",",$address);

			foreach ($arrEmails as $value) {
				$this->addAddress( trim($value), ($defaultName) ? $defaultName : null );
			}
		}
		
		$this->_mailer = \OC::$server->getMailer();
	}

	/**
	 * @param String $data
	 * @param String $filename
	 * @param String $contentType
	 * 
	 * @return MailCore
	 */
	public function addAttach( String $data, String $filename = '', String $contentType = 'application/pdf; method=request') : MailCore
	{
		$this->_attachments[] = $this->_mailer->createAttachment( $data, $filename, $contentType );
		
		return $this;
	}

	/**
	 * @param String $address
	 * @param String|null $name
	 * 
	 * @return MailCore
	 */
	public function addAddress( String $address, $name = null) : MailCore
	{
		$this->_emailAddress[$address] = $name;

		return $this;
	}

	/**
	 * @param mixed $bodyContent
	 * @param String $subject
	 * 
	 * @return Array
	 */
	public function send( $bodyContent, String $subject = '') : Array
	{
		if( !count( $this->_emailAddress ) )
		{	
			return [
				'status' => 0,
				'message' => 'Email address not register'
			];
		}

		$message = $this->_mailer->createMessage();

		$message->setTo( $this->_emailAddress );

		$message->setSubject( $subject );

		$message->setPlainBody( $bodyContent );
		
		foreach ($this->_attachments as $file ) {
			$message->attach( $file );
		}

		if( gettype($bodyContent) === 'string')
		{
			$message->setPlainBody( $bodyContent );
		}
		else if( $bodyContent instanceof TemplateResponse )
		{
			$message->setHtmlBody( $bodyContent->render() );
		}
		else
		{
			return [
				'status' => 0,
				'message' => 'templateResponse not found'
			];
		}

		$config 	= \OC::$server->getConfig();

		$address 	= $config->getSystemValue( 'mail_from_address', false );
        $domain 	= $config->getSystemValue( 'mail_domain', false );
		
		if($address && $domain)
		{
			$message->setFrom([ $address.'@'.$domain  => "Green Shield Technology"]);
		}
		
		try {
			
			$this->_mailer->send( $message );
			
			return [
				'status' => 1,
				'message' => 'Message Ok'
			];

		} catch (\Exception $e) {
			
			return [
				'status' => 0,
				'message' => $e
			];

		}
	}
}