<?php
namespace OCA\ECloud\Core;

use OC\DB\QueryBuilder\QueryBuilder;

/**
 * @example
 *
 *  $config = [
 *		'orderAvailable' => ['item' => 'products.item'],
 * 		'itemsPerPage' => 25,
 * 		'page' => 1,
 * 		'sort' => [
 * 			'name' => 'item',
 * 			'type' => 'DESC',
 * 		],
 * 		'filters' => [
 * 			'id' => 35
 * 		],
 * 	];
 * 
 *  $pagination = new \OCA\ECloud\Core\PaginationCore($config ,'products'  );
 *  $pagination->retrieve( function($qb, $pag){
 * 		if( $id = $pag->getFilter('id'))
 * 			$qb->andWhere('id = '.$id);
 * 		return $qb;
 * 	});
 *
 */
class PaginationCore{

	/**
	 * Conection Server
	 */
	private $_connection;

	private $_config;

	private $_queryBuilder = [
		'count' => null,
		'data' => null
	];

	private $_helperSQL = [
		'count' => '',
		'data' => ''
	];


	/**
	 * @param Arrray $config
	 * 
	 *		Array [orderAvailable]...... ['id' => 'product.id', 'item' => 'item.id']
	 * 
	 *		Integer [itemsPerPage]...... 10	
	 *
	 * 		Integer [page].............. 1
	 * 
	 * 		String [sort][name]......... 'id'
	 *
	 * 		String [sort][type]......... 'DESC'
	 * 
	 * 		Array  [filters]............ ['name' => 'value'] 
	 *
	 * 	@param String $tableName
	 *  @param String|Array $columns
	 *  @param \Closure $joinCallBack
	 * 
	 *  @example new 
	 */
	public function __construct( Array $config, String $tableName  = '', $columns = '*', \Closure $joinCallBack = null )
	{
		$this->_connection =  \OC::$server->getDatabaseConnection();
		$this->_config     = $config;
		
		if( $tableName )
		{
			$qbCount = $this->_connection->getQueryBuilder();
			$this->_queryBuilder['count'] = $qbCount->select( $qbCount->createFunction('COUNT(1)') )->from( $tableName );

			$qbData = $this->_connection->getQueryBuilder();
			$this->_queryBuilder['data'] = $qbData->select( $columns )->from( $tableName, $tableName  );

			if($joinCallBack && is_callable($joinCallBack) )
			{
				$this->_queryBuilder['count'] = $joinCallBack( $this->_queryBuilder['count'] );
				$this->_queryBuilder['data']  = $joinCallBack( $this->_queryBuilder['data'] );
			}
		}
	}

	/**
	 * @example
	 * 	$pagination = new PaginationCore(null, 'product');
	 * 	$pagination->selectCount(function($qBuilder){
	 * 		
	 * 		$qBuilder->select( $qBuilder->createFunction('COUNT(1)') )->from( 'tableName' );
	 * 		return $qBuilder;
	 * 		
	 * 	});
	 * 
	 * @param function $countCallBack( QueryBuilder )
	 * 
	 * @return Object
	 */
	public function selectCount( \Closure $countCallBack = null  ) : PaginationCore
	{

		if( is_callable($countCallBack) )
		{
			$qb = $this->_connection->getQueryBuilder();
			$this->_queryBuilder['count'] = $countCallBack($qb);
		}

		return $this;
	}

	/**
	 * @example
	 * 	$pagination = new PaginationCore(null, 'product');
	 * 	$pagination->selectData(function($qBuilder){
	 * 		
	 * 		$qBuilder->select( '*' )->from( 'tableName'  );
	 * 		return $qBuilder;
	 * 		
	 * 	});
	 * 
	 * @param function $dataCallBack( QueryBuilder )
	 * 
	 * @return Object
	 */
	public function selectData( \Closure $dataCallBack = null  ) : PaginationCore
	{
		if( is_callable($dataCallBack) )
		{
			$qb = $this->_connection->getQueryBuilder();
			$this->_queryBuilder['data'] = $dataCallBack($qb);
		}

		return $this;
	}

	/**
	 * @param function $conditionCallBack
	 * 
	 * @throws \InvalidArgumentException;
	 * 
	 * @return Array
	 */
	public function retrieve( \Closure $conditionCallBack = null ) : Array
	{
		$result = [
			'count' => 0,
			'data' => null
		];

		if(  !$this->_queryBuilder['count'] instanceof QueryBuilder  )
		{
			throw new \InvalidArgumentException('Error selectCount not [Select Count] QueryBuilder Register');
			//echo "Error selectCount not [Select Count] QueryBuilder Register";
			//exit;
		}
		else if(  !$this->_queryBuilder['data'] instanceof QueryBuilder  )
		{
			throw new \InvalidArgumentException('Error selectCount not [Select Data] QueryBuilder Register');
			echo "Error selectCount not [Select Data] QueryBuilder Register";
			exit;
		}

		if($conditionCallBack && is_callable($conditionCallBack))
		{
			$qbTmp = $conditionCallBack( $this->_queryBuilder['count'], 'count' );
			if($qbTmp instanceof QueryBuilder)
			{
				$this->_queryBuilder['count'] = $qbTmp;
			}
		}

		$result['count']  = $this->searchCount( $this->_queryBuilder['count'] );

		if($conditionCallBack && is_callable($conditionCallBack))
		{
			$qbTmp = $conditionCallBack($this->_queryBuilder['data'], 'data' );
			if($qbTmp instanceof QueryBuilder)
			{
				$this->_queryBuilder['data'] = $qbTmp; 
			}
		}
		
		$result['data']   = $this->searchData( $this->_queryBuilder['data'] );
		
		$result['config'] = $this->_config;

		return $result;
	}

	/**
	 * @param queryBuilder $queryBuilder
	 *
	 * @return Int
	 */
	protected function searchCount( QueryBuilder $queryBuilder ) : Int
	{
		
		$resultStatement = $queryBuilder->execute();
		$this->_helperSQL['count'] = $queryBuilder->getSQL();

		$data = $resultStatement->fetch(\PDO::FETCH_NUM);

		return $data[0];
	}

	/**
	 * @param queryBuilder $queryBuilder
	 *
	 * @return Array(Collection)
	 */
	protected function searchData( QueryBuilder $queryBuilder  ) : Array
	{

		$orderAvailable = $this->getConfig('orderAvailable', [] ); 
		$itemsPerPage   = $this->getConfig('itemsPerPage', 10 );
		$page           = $this->getConfig('page',1);

		//$queryBuilder->select( $columns )->from( $this->getTable(), $aliasTable );

		$itemsPerPage = abs($itemsPerPage);
		$page         = abs($page) - 1;
		
		if( $itemsPerPage > 0  && $page >= 0)
		{
			$queryBuilder->setMaxResults($itemsPerPage);
			$queryBuilder->setFirstResult($page * $itemsPerPage);
		}
		
		//by default ID, DESC
		$sortName = $this->getConfig('sort.name','id' );
		$sortType = $this->getConfig('sort.type','DESC');
		$sortType = strtoupper($sortType);

		if(isset($orderAvailable[$sortName]))
		{
			$sortType = in_array($sortType, ['ASC','DESC'] ) ? $sortType : 'DESC';
			$queryBuilder->orderBy($orderAvailable[$sortName],$sortType);
			$queryBuilder->addOrderBy('id','DESC');
		}
		else
		{
			$queryBuilder->orderBy('id','DESC');
		}
		$resultCollection = $queryBuilder->execute();
		$this->_helperSQL['data'] = $queryBuilder->getSQL();

		if( $resultData = $resultCollection->fetchAll(\PDO::FETCH_ASSOC) )
		{
			return $resultData;
		}
		
		return [];
	}

	/**
	 * @param String $name
	 * 
	 * @return @value
	 */
	public function getFilter( String $name = '')
	{
		$filters = $this->_config['filters'] ?? [];

		return $this->getConfig( $name, false, $filters );

	}

	/**
	 * @return Array
	 */
	public function getSQL() : Array
	{
		//
		return $this->_helperSQL;
	}

	/**
	 * @param String @name
	 * @param mixed $value
	 */
	public function setFilter( String $name , $value = null)
	{
		//
		$this->_config['filters'][$name] = $value;
	}

	/**
	 * @example
	 *  $pagination = new PaginationCore(null, 'product');
	 * 	$pagination->setConfig('orderAvailable',['id' => 'name']);
	 * 	$pagination->setConfig('itemsPerPage', 30 );
	 * 	$pagination->setConfig('page', 3 );
	 * 	$pagination->setConfig('sort',['name' => '', 'type' => '']);
	 * 	$pagination->setConfig('filters',['id' => 23]);
	 * 
	 * @param String $key
	 * @param mixed $values
	 *
	 * @return PaginationCore
	 */
	public function setConfig( String $key , $values = null ) : PaginationCore
	{
		//Available elements
			//orderAvailable
			//itemsPerPage
			//page
			//sort
			//filters
		$this->_config[$key] = $values;

		return $this;
	}

	/**
	 * Get an item from an array using "dot" notation.
	 * @example
	 *  $pagination = new PaginationCore(null, 'product');
	 * 	$pagination->getConfig('sort.name','id');
	 *  $pagination->getConfig('sort.name','id');
	 *  $pagination->getConfig('name','id',['sort' => ['name' => 'id'] ]);
	 *
	 * @param  string  $key
	 * @param  mixed   $default
	 * @param  Array   $config
	 * 
	 * @return mixed
	 */
	public function getConfig( String $key, $default = false, $config = null  )
	{
		$nameArrays = explode(".", $key);

		$keyName = $nameArrays[0];
		
		array_shift($nameArrays);

		if(is_null($config))
		{
			$config = $this->_config;
		}

		if( isset($config[$keyName]) )
		{
			if(count($nameArrays))
			{
				return $this->getConfig( implode(".", $nameArrays), $default ,$config[$keyName] );
			}
			else
			{
				return $config[$keyName];
			}
		}
		else
		{
			return $default;
		}
	}
}