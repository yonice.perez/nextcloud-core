<?php
namespace OCA\ECloud\Core;

use OC\DB\QueryBuilder\QueryBuilder;

class ModelCore {
	
	/**
	 * @const String Replace table name and ignore value
	 */
	const REPLACE_TABLE = "/Model\z/i";

	/**
	 * 
	 */
	private static $_instance   = null;
	
	/**
	 * 
	 */
	private $_tablename  = '';
	
	/**
	 * 
	 */
	private $_connection = null;
	
	/**
	 * 
	 */
	private $_data = null;

	/**
	 * type = ROW
	 * type = ARRAY
	 */
	private $_data_type = '';

	
	public function __construct( $tableName = null )
	{
		$this->_connection =  \OC::$server->getDatabaseConnection();

		if(!$tableName)
		{
			$tableName = basename( str_replace("\\","/", get_class( $this ) ) );
		}

		if(self::REPLACE_TABLE)
		{
			$tableName = strtolower(
				preg_replace(self::REPLACE_TABLE, "", $tableName  )
			);
		}

		$this->_tablename = $tableName;
			
		
	}

	/**
	 * 
	 */
	public static function init()
	{

		if(self::$_instance == null)
		{

			$calledClass = get_called_class();
			
			$parentModel = basename( str_replace("\\","/", $calledClass ) );
			
			$instance    = new ModelCore( $parentModel );

			self::$_instance = $instance;

		}

		return self::$_instance;
	}

	/**
	 * 
	 */
	public static function setTable( String $tableName  ) : ModelCore
	{
		self::init();
		
		self::$_instance->_tablename = $tableName;
		
		return self::$_instance;
	}

	public static function getTable() : String
	{
		self::init();

		return self::$_instance->_tablename;
	}

	/**
	 * @return QueryBuilder
	 */
	public static function getQueryBuilder() : QueryBuilder
	{
		self::init();

		return self::$_instance->_connection->getQueryBuilder();
	}
	
	/**
	 * @param Array $params
	 * 
	 * @return ModelCore
	 */
	public static function insert( $params ) : ModelCore
	{
		self::init();
		
		$query = self::getQueryBuilder();
		$query->insert( self::getTable() );
		foreach($params as $column => $value) {
			$query->setValue($column, $query->createNamedParameter($value));
		}
		$query->execute();
		
		return self::get( $query->getLastInsertId() );
	}

	/**
	 * @param Array|Int $filter
	 * 
	 * @return ModelCore
	 */
	public static function get( $filter = null ) : ModelCore
	{
		self::init();

		self::$_instance->_data_type = 'ROW';

		$query = self::getQueryBuilder();
		
		$query->select( '*' )
			->from( self::getTable() );
		
		if(is_numeric($filter))
		{
			$query->andWhere(  $query->expr()->eq('id', $query->createNamedParameter($filter) ));
		}
		else if( is_array($filter) )
		{
			foreach ($filter as $key => $value) {
				$query->andWhere(  $query->expr()->eq($key, $query->createNamedParameter($value) ));
			}
		}

		$stmt   = $query->execute();
		$result = $stmt->fetch(\PDO::FETCH_ASSOC);
		
		self::$_instance->setResult($result);
		
		return self::$_instance;
	}

	/**
	 * @param Array $filter
	 * 
	 * @return ModelCore
	 */
	public static function getAll( $filter = [] ) : ModelCore
	{
		self::init();

		self::$_instance->_data_type = 'ARRAY';

		$query = self::getQueryBuilder();
		
		$query->select( '*' )->from( self::getTable() );
		
		if( is_array($filter) )
		{
			foreach ($filter as $key => $value) {
				$query->andWhere( 
					$query->expr()->eq( $key, $query->createNamedParameter($value) ) 
				);
			}
		}
		
		$stmt   = $query->execute();

		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		
		self::$_instance->setResult($result);
		
		return self::$_instance;
	}

	/**
	 * @param Array $result
	 * 
	 * @return avoid
	 */
	public static function setResult( $result = null  )
	{
		self::init();
		
		self::$_instance->_data = $result;
	}

	/**
	 * @return Array
	 */
	public static function result()
	{
		self::init();

		return self::$_instance->_data;
	}
	
	/**
	 * @param Array $params
	 * 
	 */
	public function update( Array $params = [] , $id = false )
	{

		$query = $this->getQueryBuilder();
		
		$query->update( $this->getTable() );

		if(!count($params))
		{
			return false;
		}
		
		foreach ( $params as $fieldName => $value) {
			
			$this->_updateData( $params );

			$query->set( $fieldName, $query->createNamedParameter( $value ) );
		}

		$query = $this->_queryWhere( $query, $id );

		if( $query instanceof QueryBuilder)
		{
			return $query->execute();
		}

		return false;
	}
	
	/**
	 * 
	 */
	public function delete( $id = false )
	{
		//self::init();

		$query = $this->getQueryBuilder();
		
		$query->delete( $this->getTable() );
		
		$query = $this->_queryWhere( $query, $id );

		if( $query instanceof QueryBuilder )
		{
			$this->setResult( null );
			
			return $query->execute();
		}
	}

	/**
	 * 
	 */
	private function _queryWhere( $query, $id = FALSE )
	{
		$data = $this->result();
		
		if($id)
		{
			$query->where($query->expr()->eq('id', $query->createNamedParameter($id)));
		}
		else if(  self::$_instance->_data_type === 'ROW' && isset($data['id']) )
		{
			$query->where($query->expr()->eq('id', $query->createNamedParameter( $data['id']  )));
		}
		else if(  self::$_instance->_data_type === 'ARRAY' && count($data)>0 )
		{
			foreach ($data as $value) {
				$query->orWhere( $query->expr()->eq('id', $query->createNamedParameter( $value['id'] ) ) );
			}
		}
		else
		{
			return null;
		}
		return $query;
	}

	/**
	 * 
	 */
	private function _updateData( $valueData = null )
	{
		$data = $this->result();

		if(  $this->_data_type === 'ROW' )
		{
			$this->_data = array_merge( $data , $valueData );
		}
		else if(  $this->_data_type === 'ARRAY' && count($data)>0 )
		{
			foreach ($data as $key => $value) {
				$this->_data[$key] = array_merge($value, $valueData );
			}
		}
	}

}

