<?php
namespace OCA\ECloud\Core;

use OCP\IRequest;
use OCP\AppFramework\Controller;
use OCP\AppFramework\App;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\IUser;
use OCP\IUserSession;

class ControllerCore extends Controller {

	/**
	 * 
	 */
	public $IManager;

	/**
	 * 
	 */
	public $IUserManager;

	/**
	 * 
	 */
	public $IMailer;

	/**
	 * 
	 */
	public $IGroupManager;

	/**
	 * 
	 */
	public $IURLGenerator;

	/**
	 * 
	 */
	public $IConfig;

	/**
	 * User Collection
	 * getUID()
	 * getDisplayName()
	 * getEMailAddress()
	 * isEnabled()
	 */
	private $currentUser = null;

	public function __construct( $AppName, IRequest $request) {
		
		parent::__construct( $AppName, $request);
		
		$app       = new App( $AppName );
		$container = $app->getContainer();
		
		$this->IManager      = $container->query('OCP\Notification\IManager');
		$this->IUserManager  = $container->query('OCP\IUserManager');
		$this->IMailer       = $container->query('OCP\Mail\IMailer');
		$this->IGroupManager = $container->query('OCP\IGroupManager');
		$this->IURLGenerator = $container->query('OCP\IURLGenerator');
		$this->IConfig       = $container->query('OCP\IConfig');
		$this->IUserSession  = $container->query('OCP\IUserSession');
		
		$user = $this->IUserSession->getUser();

		if ($user instanceof IUser) 
		{
			$this->currentUser = $user;
		} 

		include __DIR__ . '/../../vendor/wixel/gump/gump.class.php';
			
	}

	/**
	 * @return TemplateResponse
	 */
	public function getTemplate( String $nameTemplate, Array $params = [] ) : TemplateResponse
	{
		return new TemplateResponse( 'ecloud', $nameTemplate, $params);
	}

}
